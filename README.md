
# ZEngine
[![MIT License](https://img.shields.io/badge/License-MIT-green.svg)](https://choosealicense.com/licenses/mit/)


Game engine on PyGame and Python


## Demo

![ZEngine Public Build](https://i.ibb.co/J3kTCCp/image.png)
Code:
```python
import pygame
from engine import ZEngine
from pygame.color import THECOLORS
import time

engine = ZEngine('ZEngine Test', 60 ,600, 600)


engine.fill(0, 0, 0)

# draw shapes
engine.draw('rect', THECOLORS['orange'], (50, 50), (200, 100))
time.sleep(0.6)

engine.draw('polygon', THECOLORS['purple'], [(250, 250), (300, 200), (400, 300)], (30,30))
time.sleep(0.6)

engine.draw('circle', THECOLORS['white'], (500, 300), 50)
time.sleep(0.6)

engine.draw('line', THECOLORS['red'], [(500, 500), (650, 650)], 5)
time.sleep(0.6)

engine.paste_image("test.jpg", (0,400), (200,200))
time.sleep(0.6)

engine.show()

engine.run()
```
Animated test:
![Engine Anim Work](https://s6.gifyu.com/images/S8RUW.gif)
## Documentation

[Documentation on Russian Language](https://savory-wolfsbane-216.notion.site/b3b240d65fc34e258bb50360c80734c5?pvs=4)


## Authors

- [@Syirezz_Developer](https://codeberg.org/Syirezz_Developer)

