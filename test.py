import pygame
from engine import ZEngine
from pygame.color import THECOLORS

engine = ZEngine('ZEngine Test', 600, 600)

engine.fill(0, 0, 0)

# draw shapes
engine.draw('rect', THECOLORS['orange'], (50, 50), (200, 100))
engine.draw('polygon', THECOLORS['purple'], [(250, 250), (300, 200), (400, 300)], (30,30))
engine.draw('circle', THECOLORS['white'], (500, 300), 50)
engine.draw('line', THECOLORS['red'], [(500, 500), (650, 650)], 5)

engine.paste_image("test.jpg", (0,400), (200,200))
# display shapes

player = engine.draw('rect', THECOLORS['blue'], (20, 20), (10, 10))
# engine.player_movement(player, 5, 10, 10)

engine.show()

# main game loop
engine.run()