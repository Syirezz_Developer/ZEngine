import pygame
from pygame.color import THECOLORS
import os
import sys

pygame.init() #Init pygame for engine

class ZEngine():
    def __init__(self, title = "ZEngine" ,window_size_x = 100, window_size_y = 100):
        self.title = title
        self.window_size_x = window_size_x
        self.window_size_y = window_size_y
        self.screen = pygame.display.set_mode((self.window_size_x, self.window_size_y))

    def fill(self, r, g, b):
        self.screen.fill((r, g, b))

    def show(self):
        pygame.display.set_caption(self.title)
        pygame.display.update()

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

    def draw(self, shape, color, pos, size):
        if shape == 'rect':
            pygame.draw.rect(self.screen, color, (pos[0], pos[1], size[0], size[1]))
        elif shape == 'polygon':
            pygame.draw.polygon(self.screen, color, pos)
        elif shape == 'circle':
            pygame.draw.circle(self.screen, color, pos, size)
        elif shape == 'line':
            pygame.draw.line(self.screen, color, pos[0], pos[1], size)
        else:
            print(f'ZEngine: could not find a shape with name {shape}')

    def paste_image(self, img_path, pos, size=None):
        img = pygame.image.load(img_path)
        if size:
            img = pygame.transform.scale(img, size)
        self.screen.blit(img, pos)

    def player_movement(self, Player):
      keys = pygame.key.get_pressed()
      if keys[pygame.K_LEFT]:
          Player.x -= Player.speed
      if keys[pygame.K_RIGHT]:
          Player.x += Player.speed
      if keys[pygame.K_UP]:
          Player.y -= Player.speed
      if keys[pygame.K_DOWN]:
          Player.y += Player.speed

    